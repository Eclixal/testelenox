<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Urb\XenforoBridge\XenforoBridge;

class Test extends Controller {

    protected $xenforo;

    protected $item;

    public function __construct(XenforoBridge $xenforo) {
        $this->xenforo = $xenforo;
    }

    public function index(Request $request) {
        dd($this->xenforo->getVisitor());
    }
}
